//Zad.1

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp6
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
        }

        double op, num;
        int count;
        private void button0_Click(object sender, EventArgs e)
        {
           
            textBox1.Text = textBox1.Text + 0;
            label1.Text = label1.Text + '0';
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 1;
            label1.Text = label1.Text + '1';

        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 2;
            label1.Text = label1.Text + '2';

        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 3;
            label1.Text = label1.Text + '3';

        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 4;
            label1.Text = label1.Text + '4';
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 5;
            label1.Text = label1.Text + '5';

        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 6;
            label1.Text = label1.Text + '6';

        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 7;
            label1.Text = label1.Text + '7';
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 8;
            label1.Text = label1.Text + '8';

        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + 9;
            label1.Text = label1.Text + '9';

        }

        private void buttontocka_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + '.';
            label1.Text = label1.Text + '.';

        }

        private void buttonplus_Click(object sender, EventArgs e)
        {

            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 1;
            label1.Text = num.ToString() + "+";


        }

        private void buttonjendako_Click(object sender, EventArgs e)
        {

            calculate();
            label1.Text = "";
            
        }

        private void buttonputa_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 3;
            label1.Text = op.ToString() + '*';
        }

        private void buttonpodij_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 4;
            label1.Text = op.ToString() + '/';
        }

        private void buttonminus_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 2;
            label1.Text = op.ToString() + '-';
        }

        private void buttonclear_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void buttonkorjen_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 9;
            label1.Text = "?"+ op.ToString();
        }

        private void buttonvcos_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 5;
            label1.Text = "cos" + op.ToString();
        }

        private void buttonlog_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 8;
            label1.Text = "ln" + op.ToString();
        }

        private void buttontan_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 6;
            label1.Text = "tan" + op.ToString();
            
        }

        private void buttonsin_Click(object sender, EventArgs e)
        {
            op = double.Parse(textBox1.Text);
            textBox1.Clear();
            textBox1.Focus();
            count = 5;
            label1.Text = "sin" + op.ToString();
        }

        public void calculate()
        {
            switch (count)
            {
                case 1:
                    num = op + double.Parse(textBox1.Text);
                    textBox1.Text = num.ToString();
                    break;
                case 2:
                    num = op - double.Parse(textBox1.Text);
                    textBox1.Text = num.ToString();
                    break;
                case 3:
                    num = op * double.Parse(textBox1.Text);
                    textBox1.Text = num.ToString();
                    break;
                case 4:
                    num = op / double.Parse(textBox1.Text);
                    textBox1.Text = num.ToString();
                    break;
                case 5:
                    num = Math.Cos(op);
                    textBox1.Text = num.ToString();
                    break;
                case 6:
                    num = Math.Tan(op);
                    textBox1.Text = num.ToString();
                    break;
                case 7:
                    num = Math.Sin(op);
                    textBox1.Text = num.ToString();
                    break;

                case 8:
                    num = Math.Log(op);
                    textBox1.Text = num.ToString();
                    break;
                case 9:
                    num = Math.Sqrt(op);
                    textBox1.Text = num.ToString();
                    break;
                default:
                    break;
            }


        }
    }
}

//Zad.2

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {
        List<string> list = new List<string>();
        string path = "lista.txt";
        int counter = 7;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) 
                {
                    
                    
                    list.Add(line);
                }
                listBox1.DataSource = null;
                listBox1.DataSource = list;
                listBox1.Hide();
               
            }
        }

      
        private void button2_Click(object sender, EventArgs e)
        {

            bool found = false;
            string word = listBox1.SelectedItem.ToString();
            string letter = textBox1.Text;
            if (word.Substring(0, word.Length - 1).Contains(letter))
            {
                found = true;
            }
            else
            {
                found = false;
                label1.Text = (--counter).ToString();
                if (counter == 0)
                {
                    MessageBox.Show("Izgubili ste");
                    counter = 7;
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random generator = new Random();
            int var = listBox1.Items.Count;
            int i = generator.Next(0, var);
            listBox1.SetSelected(i, true);
            string a = listBox1.SelectedItem.ToString();
            label2.Text = "Duljina Rijeci: "+a.Length.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string a = listBox1.SelectedItem.ToString();
            string b = textBox2.Text;
            if (a==b)
            {
                MessageBox.Show("Pogodili ste");
            }

        }

       
    }
 }
